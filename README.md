# ENM

Cours, TD et TP de l'ENM.

Pour récupérer les sources, il faut cloner ce dépot git. Faire la commande suivante dans un terminal :

```bash
git clone https://gitlab.irit.fr/toc/mathn7/etudiants/enm.git
```

Vous pouvez ensuite renommer le répertoire parent. Une autre possibilité est de le faire directement par le clone :

```bash
git clone repo-name folder-name
```
